'use strict';

angular.module('SmartSpender')
  // use factory for services
  .factory('ExpenseTypeIconMap', function(ExpenseType) {

    var data = {};

    data[ExpenseType.FOOD_DRINK] = 'images/food.svg';
    data[ExpenseType.FUN] = 'images/fun.svg';
    data[ExpenseType.BILLS] = 'images/bills.svg';
    data[ExpenseType.CLOTHES] = 'images/clothes.svg';
    data[ExpenseType.HEALTH] = 'images/health.svg';
    data[ExpenseType.OTHER] = 'images/other.svg';

    return data;

  });
