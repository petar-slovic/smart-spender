'use strict';

angular.module('SmartSpender')
  // use factory for services
  .factory('ExpenseType', function() {

    var data = {
    	'FOOD_DRINK': '1',
    	'FUN': '2',
    	'BILLS': '3',
    	'CLOTHES': '4',
    	'HEALTH': '5',
    	'OTHER': '6'
    };

    return data;

  });
