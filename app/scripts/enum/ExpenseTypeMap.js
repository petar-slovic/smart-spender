'use strict';

angular.module('SmartSpender')
  // use factory for services
  .factory('ExpenseTypeMap', function() {

    var data = {
    	1: 'Food/Drink',
    	2: 'Fun',
    	3: 'Bills',
    	4: 'Clothes',
    	5: 'Health',
    	6: 'Other'
    };

    return data;

  });
