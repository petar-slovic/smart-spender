'use strict';

angular.module('SmartSpender')
  // use factory for services
  .factory('CalculatorState', function() {

    var data = {
    	'DECIMAL_POINT': 'decimal-point',
    	'NO_DECIMAL_POINT': 'no-decimal-point',
    };

    return data;

  });
