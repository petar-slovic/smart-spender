'use strict';

/**
 * @ngdoc function
 * @name SmartSpender.serive:ExampleService
 * @description
 * # ExampleService
 */
angular.module('SmartSpender')
  // use factory for services
  .factory('ExpensesService', function(_, DataService, Expense) {

    function getMonthlySum(momentMonth) {
        var expenses = Expense.getAll();
        return _(expenses)
            .filter(function(expense) {
                return momentMonth.format('MM YYYY') === moment(expense.date).format('MM YYYY');
            })
            .reduce(function(sum, expense) {
                return sum + parseInt(expense.amount, 10);
            }, 0);
    }

    return {
        getMonthlySum: getMonthlySum
    }

  });
