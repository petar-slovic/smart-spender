'use strict';

angular.module('SmartSpender')
  .factory('Settings', function(Store) {

    var Settings = Store.defineResource({
      name: 'settings',
      methods: {
        save: function() {
          return this.DSLastSaved() ? this.DSSave() : this.DSCreate();
        }
      }
    });

    return Settings;

  });
