'use strict';

angular.module('SmartSpender')
  .factory('Expense', function(Store) {

    var Expense = Store.defineResource({
  	  name: 'expense',
  	  methods: {
  	  	save: function() {
  	  		return this.DSLastSaved() ? this.DSSave() : this.DSCreate();
  	  	}
  	  }
  	});

    function getAllMonth(month) {
      var whereClause = {
        orderBy: [[
          'date', 'DESC'
        ]]
      }
      return _.filter(Expense.filter(whereClause), function(expense) {
        return moment(expense.date).format('MM YYYY') === month.format('MM YYYY');
      });
    }

    function findAllMonth(month) {
      return Expense.findAll().then(function(expenses) {
        return _(expenses)
        .filter(function(expense) {
          return moment(expense.date).format('MM YYYY') === month.format('MM YYYY');
        })
        .sortBy(function(expense) {
          return -expense.date.valueOf();
        })
        .value();
      });
    }

    Expense = angular.extend(Expense, {
      getAllMonth: getAllMonth,
      findAllMonth: findAllMonth
    });

    return Expense;

  });
