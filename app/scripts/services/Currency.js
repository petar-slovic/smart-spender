'use strict';

angular.module('SmartSpender')
  .factory('Currency', function($http) {
    var currencies = currencyData;

    function getAll() {
      return currencies;
    }

    function getByCode(code) {
      return currencies[code];
    }

    return {
      getAll: getAll,
      getByCode: getByCode
    };
  });
