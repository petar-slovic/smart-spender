'use strict';

angular.module('SmartSpender')
  .factory('Store', function(DS) {

    var adapter = new DSLocalStorageAdapter();
    var store = new JSData.DS();
    store.registerAdapter('localstorage', adapter, { default: true });

    return store;

  });
