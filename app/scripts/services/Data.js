'use strict';

/**
 * @ngdoc function
 * @name SmartSpender.serive:ExampleService
 * @description
 * # ExampleService
 */
angular.module('SmartSpender')
  .factory('DataService', function(ExpensesViewModel, SettingsModel) {

    var data = {};

    data.expensesViewModel = new ExpensesViewModel();
    data.settingsModel = SettingsModel;

    return data;

  });
