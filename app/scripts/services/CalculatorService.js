'use strict';

angular.module('SmartSpender')
  .factory('CalculatorService', function($filter, CalculatorState) {

    var acc = '0';
    var state = CalculatorState.NO_DECIMAL_POINT;
    var pendingDot = false;

    function append(number) {

      if(acc === '0' && !pendingDot) {
        acc = '' + number;
        return;
      }

      var prefix = '';
      if(pendingDot) {
        prefix = '.';
        pendingDot = false;
      }

      acc += prefix + number;
    }

    function getValue() {
      // return $filter('number')(acc, 2);
      return acc;
    }

    function setValue(value) {
      acc = '' + value;
      pendingDot = false;

      if(Math.floor(value) !== value) {
        state = CalculatorState.DECIMAL_POINT;
      }
    }

    function reset() {
      acc = '0';
      pendingDot = false;
      state = CalculatorState.NO_DECIMAL_POINT;
    }

    function removeRight() {
      acc = acc.length > 1 ? acc.slice(0, -1) : '0';
      if(acc[acc.length - 1] === '.') {
        acc = acc.slice(0, -1);
      }

      state = CalculatorState[
        Math.floor(parseFloat(acc, 10)) === parseFloat(acc, 10) ?
          'NO_DECIMAL_POINT':
          'DECIMAL_POINT'
      ];
      pendingDot = false;
    }

    function putDecimalPoint() {
      state = CalculatorState.DECIMAL_POINT;
      pendingDot = true;
    }

    return {
      append: append,
      getValue: getValue,
      setValue: setValue,
      reset: reset,
      removeRight: removeRight,
      putDecimalPoint: putDecimalPoint
    }

  });
