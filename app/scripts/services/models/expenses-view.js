'use strict';

angular.module('SmartSpender')
  .value('ExpensesViewModel', function() {

    return {
    	activeMonth: (new Date()).getMonth() + 1
    }

  });
