'use strict';

angular.module('SmartSpender')
  .factory('SettingsModel', function($q, Settings, Currency) {
    var settings;

    var defaultSettings = {
      showCurrency: true,
      currency: Currency.getByCode('USD'),
      headerImage: 'header/header-wood.jpg'
    };

    function getInstance() {
      if(!settings) {
        return Settings.findAll().then(function(allSettings) {
          if(allSettings.length) {
            settings = allSettings[0];
          } else {
            settings = Settings.createInstance(defaultSettings);
            settings.save();
          }
          return $q.when(settings);
        });
      }
      return $q.when(settings);
    }

    function setProperty(prop, value) {
      settings[prop] = value;
      return settings.save();
    }

    function save() {
      return settings.save();
    }

    return {
      getInstance: getInstance,
      set: setProperty,
      save: save
    };

  });
