'use strict';

/**
 * @ngdoc overview
 * @name SmartSpender
 * @description
 * # Initializes main application and routing
 *
 * Main module of the application.
 */


angular.module('SmartSpender', ['ionic', 'ngCordova', 'ngResource', 'js-data', 'chart.js'])

.run(function($ionicPlatform, ExpenseType, Expense, DataService, _, $cordovaStatusbar, $state, $ionicPopup, $ionicHistory) {

  $ionicPlatform.ready(function() {
      // $cordovaStatusbar.overlaysWebView(false);
      // $cordovaStatusbar.styleHex('#F17859')

      // $ionicAppProvider.identify({
      //   // The App ID (from apps.ionic.io) for the server
      //   app_id: '2294d1fa',
      //   // The public API key all services will use for this app
      //   api_key: '40a26681006fc594e3c78a54e96546c303b096b4d87f703c',
      //   // The GCM project ID (project number) from your Google Developer Console (un-comment if used)
      //   // gcm_id: 'YOUR_GCM_ID'
      // });

      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }

      ionic.Platform.isFullScreen = true;

      $ionicPlatform.registerBackButtonAction(function(event) {
        if ($state.is('app.expenses.all')) {
          var confirmPopup = $ionicPopup.confirm({
            title: 'Exit?',
            template: 'Are you sure you want to exit?'
          });
          confirmPopup.then(function(res) {
            if (res) {
              navigator.app.exitApp();
            }
          });
        } else {
          $ionicHistory.goBack()
        }
      }, 100);
  });

DataService.expensesViewModel.activeMonth = moment();

Waves.init({
  duration: 150,
  delay: 50
});

})

.config(function($httpProvider, $stateProvider, $urlRouterProvider, $compileProvider) {
  // register $http interceptors, if any. e.g.
  // $httpProvider.interceptors.push('interceptor-name');

  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//);

  // Application routing
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      views: {
        'viewIndex': {
          templateUrl: 'templates/main.html',
          controller: 'MainController'
        }
      }
    })
    .state('app.home', {
      url: '/home',
      cache: true,
      views: {
        'viewContent': {
          templateUrl: 'templates/views/home.html',
          controller: 'HomeController'
        }
      }
    })
    .state('app.stats', {
      url: '/stats',
      cache: false,
      views: {
        'viewContent': {
          templateUrl: 'templates/views/stats.html',
          controller: 'StatsController'
        }
      },
      resolve: {
        settings: function(DataService) {
          return DataService.settingsModel.getInstance();
        }
      },
      onEnter: function($rootScope, Expense) {
        $rootScope.$broadcast('expense:reload');
      }
    })
    .state('app.info', {
      url: '/info',
      cache: true,
      views: {
        'viewContent': {
          templateUrl: 'templates/views/info.html',
          controller: function() {}
        }
      }
    })
    .state('app.settings', {
      url: '/settings',
      cache: true,
      views: {
        'viewContent': {
          templateUrl: 'templates/views/settings.html',
          controller: 'SettingsController'
        }
      },
      resolve: {
        settings: function(DataService) {
          return DataService.settingsModel.getInstance();
        }
      }
    })
    .state('app.settingsHeaderImage', {
      url: '/settings/header-image',
      cache: true,
      views: {
        'viewContent': {
          templateUrl: 'templates/views/settings-header-image.html',
          controller: 'SettingsController'
        }
      },
      resolve: {
        settings: function(DataService) {
          return DataService.settingsModel.getInstance();
        }
      }
    })
    .state('app.expenses', {
      url: '/expenses',
      abstract: true,
      views: {
        'viewContent': {
          templateUrl: 'templates/views/expenses.html',
          controller: function($scope, $rootScope, Expense, DataService, expenses, settings) {
            $scope.expenses = expenses;
            $scope.settings = settings;

            $rootScope.$on('expense:reload', function(ev) {
              $scope.expenses = Expense.getAllMonth(DataService.expensesViewModel.activeMonth);
            });
          }
        }
      },
      resolve: {
        expenses: function($q, Expense, DataService) {
          return $q.when(Expense.findAllMonth(DataService.expensesViewModel.activeMonth));
        },
        settings: function(DataService) {
          return DataService.settingsModel.getInstance();
        }
      }
    })
    .state('app.expenses.all', {
      url: '/all',
      views: {
        'expenses': {
          templateUrl: 'templates/views/expenses-all.html',
          controller: function($scope, $rootScope, $state, ExpenseTypeIconMap, Expense) {
            $scope.ExpenseTypeIconMap = ExpenseTypeIconMap;
          }
        }
      },
      onEnter: function($rootScope, Expense) {
        $rootScope.$broadcast('expense:reload');
        $rootScope.$broadcast('expenses-header:recalculate-sum');
      }
    })
    .state('app.expenses.one', {
      url: '/:id',
      cache: true,
      abstract: true,
      views: {
        'expenses': {
          templateUrl: 'templates/views/expense.html',
          controller: function($scope) {}
        }
      },
      resolve: {
        expense: function($stateParams, Expense) {
          return Expense.find($stateParams.id);
        }
      }
    })
    .state('app.expenses.new', {
      url: '/:id/new',
      cache: false,
      abstract: true,
      views: {
        'expenses': {
          templateUrl: 'templates/views/expense.html',
          controller: function($scope, $state, $stateParams, expense, ExpenseTypeMap, ExpenseTypeIconMap) {
            $scope.ExpenseTypeMap = ExpenseTypeMap;
            $scope.ExpenseTypeIconMap = ExpenseTypeIconMap;
            $scope.expense = expense;
            $scope.isNew = !$stateParams.id;
            $scope.deleteExpense = function(expense) {
              expense.DSDestroy()
                .then(function() {
                  $state.go('app.expenses.all');
                });

            };

            $scope.choseDate = function(expense) {
              datePicker.show({
                date: new Date(),
                mode: 'date'
              }, function(date) {
                if(date) {
                  $scope.$apply(function() {
                    $scope.expense.date = date;
                  });

                }
              });
            };
          }
        }
      },
      resolve: {
        expense: function($stateParams, Expense, DataService) {
          if ($stateParams.id) {
            return Expense.find($stateParams.id);
          } else {
            return Expense.createInstance({
              date: moment().valueOf(),
              amount: 0
            });
          }
        }
      }
    })
    .state('app.expenses.new.amount', {
      url: '/amount',
      cache: true,
      views: {
        'expense': {
          templateUrl: 'templates/views/expenses-new-amount.html',
          controller: function($scope, CalculatorService) {
            $scope.vm = {};

            CalculatorService.setValue($scope.expense.amount);

            $scope.$watch(function() {
              return CalculatorService.getValue();
            }, function(newValue, oldValue) {
              $scope.expense.amount = newValue;
            });

            $scope.vm.append = CalculatorService.append;
            $scope.vm.removeRight = CalculatorService.removeRight;
            $scope.vm.putDecimalPoint = CalculatorService.putDecimalPoint;
          },
          controllerAs: 'vm'
        }
      }
    })
    .state('app.expenses.new.type', {
      url: '/type',
      cache: true,
      views: {
        'expense': {
          templateUrl: 'templates/views/expenses-new-type.html',
          controller: function($scope, $state, expense) {
            $scope.selectType = function(id) {
              $scope.expense.type = id;
              $state.go('app.expenses.new.description', {
                id: expense.id
              });
            }

            var expenseTypeArray = _.map($scope.ExpenseTypeMap, function(type, id) {
              return {
                id: id,
                type: type
              }
            });
            $scope.expenseTypesGroupped = _.chunk(expenseTypeArray, 3);
          }
        }
      }
    })
    .state('app.expenses.new.description', {
      url: '/description',
      cache: true,
      views: {
        'expense': {
          templateUrl: 'templates/views/expenses-new-description.html',
          controller: function($scope, $rootScope, $state, Expense) {
            $scope.save = function(expense) {
              expense
                .save()
                .then(function() {
                  $state.go('app.expenses.all');
                });
            }
          }
        }
      }
    });


  // redirects to default route for undefined routes
  $urlRouterProvider.otherwise('/app/expenses/all');

});
