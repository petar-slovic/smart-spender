'use strict';

/**
 * @ngdoc function
 * @name SmartSpender.controller:MainController
 * @description
 * # MainController
 */
angular.module('SmartSpender')
  .controller('MainController', function($scope) {

    // do something with $scope
    console.log('main controller');
  });
