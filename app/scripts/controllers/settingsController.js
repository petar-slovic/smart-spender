'use strict';

/**
 * @ngdoc function
 * @name SmartSpender.controller:SettingsController
 * @description
 * # SettingsController
 */
angular.module('SmartSpender')
  .controller('SettingsController', function($scope, DataService, Currency, $state, settings) {

    $scope.settings = settings;

    $scope.vm = {
      currencies: Currency.getAll(),
      selectedCurrency: _.findWhere(Currency.getAll(), {code: settings.currency.code}),
      showCurrency: settings.showCurrency,
      headerImages: headerImages
    };

    $scope.vm.saveSettings = function(prop, value) {
      DataService.settingsModel.set(prop, value);
    };

    $scope.vm.saveHeaderImage = function(image) {
      DataService.settingsModel.set('headerImage', image)
        .then(function() {
          $state.go('app.expenses.all');
        });
    }

  });
