'use strict';

/**
 * @ngdoc function
 * @name SmartSpender.controller:StatsController
 * @description
 * # SettingsController
 */
angular.module('SmartSpender')
  .controller('StatsController', function($scope, $rootScope, DataService, Expense, Currency, $state, ExpenseTypeMap, settings) {
    $scope.settings = settings;

    var chartConfig = {
      animationEasing: 'easeOutBack',
      animationSteps: 20,
      responsive: true,
    }

    $scope.vm = {
      chartConfig: chartConfig
    };

    $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Something"];
    $scope.data = [300, 500, 100, 340];

    $scope.vm.getExpensesForMonth = function(month) {
      Expense.findAllMonth(month)
        .then(function(expenses) {
          $scope.vm.chartData = getExpensesFormatted(expenses);
        });
    }

    $scope.vm.getExpensesAll = function() {
      $scope.vm.expenses = Expense.getAll();
      $scope.vm.chartData = getExpensesFormatted($scope.vm.expenses);
      $scope.vm.monthAverage = calculateMonthAverage($scope.vm.expenses);
    }
    // $scope.vm.getExpensesForMonth(DataService.expensesViewModel.activeMonth);
    $scope.vm.getExpensesAll();

    function getExpensesFormatted(expenses) {
      var typeSums =
        _(expenses)
        .groupBy(function(expense) {
          return expense.type;
        })
        .map(function(expenses, type) {
          var sum = _.sum(expenses, function(expense) {
            return expense.amount;
          });

          return {
            type: type,
            sum: sum
          }
        })
        .value();

      var labels = _.pluck(typeSums, 'type').map(function(type) {
        return ExpenseTypeMap[type];
      });

      var sums = _.pluck(typeSums, 'sum');

      return {
        labels: labels,
        sums: sums
      };
    }

    function calculateMonthAverage(expenses) {
      var monthlySums = _(expenses)
        .groupBy(function(expense) {
          return moment(expense.date).format('MM YYYY');
        })
        .map(function(expenses, month) {
          return _.sum(expenses, function(expense) {
            return expense.amount;
          });
        })
        .value();
      return _.sum(monthlySums) / monthlySums.length;
    }

    $rootScope.$on('expense:reload', function() {
      $scope.vm.getExpensesAll();
    });

  });
