'use strict';

angular.module('SmartSpender')
  .directive('waves', [function() {
    return {
      restrict: 'A',
      scope: {
        waveClasses: '='
      },
      link: function(scope, element) {
        var classes = scope.waveClasses ? scope.waveClasses.split(' ') : [];
        Waves.attach(element, classes);
      }
    };
  }]);
