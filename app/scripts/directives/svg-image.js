'use strict';

angular.module('SmartSpender')
  .directive('svgImage', ['$http', function($http) {
    return {
      restrict: 'E',
      scope: {
        src: '='
      },
      link: function(scope, element) {
        var imgURL = scope.src;
        scope.$watch('src', function(newSrc, oldSrc) {
          $http.get(
            newSrc, {
              'Content-Type': 'application/xml'
            }
          ).success(function(data) {
            element.empty();
            element.append(scope.manipulateImgNode(data, element));
          });
        })

        scope.manipulateImgNode = function(data, elem) {
          var $svg = angular.element(data)[4];
          var imgClass = elem.attr('class');
          $svg.removeAttribute('xmlns:a');
          return $svg;
        };
      }
    };
  }]);
