'use strict';

(function() {

  var app = angular.module('SmartSpender');

  app.directive('expensesHeader', function () {

      var controller = function ($rootScope, DataService, ExpensesService) {
        var vm = this;
        vm.currentMonth = DataService.expensesViewModel.activeMonth.format('MMMM, YYYY.');
        vm.expensesSum = calculateSum();

        vm.goMonth = goMonth;

        function calculateSum() {
          return ExpensesService.getMonthlySum(DataService.expensesViewModel.activeMonth);
        }

        function goMonth(monthNumber) {
          DataService.expensesViewModel.activeMonth = DataService.expensesViewModel.activeMonth.add(monthNumber, 'months');
          vm.currentMonth = DataService.expensesViewModel.activeMonth.format('MMM, YYYY.');
          vm.expensesSum = calculateSum();
          $rootScope.$broadcast('expense:reload', DataService.expensesViewModel.activeMonth);
        }

        $rootScope.$on('expenses-header:recalculate-sum', function() {
          vm.expensesSum = calculateSum();
        });

      };

      return {
          restrict: 'EA', //Default for 1.3+
          scope: {
            headerImage: '=',
            currencySymbol: '='
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          templateUrl: 'templates/directives/expenses-header.html'
      };
  });

}());
